import pygame
import random

red = (255, 0, 0)
green = (0, 255, 0)
zombie_size = (50, 50)
rand = [0, 750]
center = (400, 400)
max_hp = 3

# If spawn super zombie, here are the new attributes: 
super_zombie_hp = 10
super_zombie_size = (100, 100)

speed = float(1000) # zombies reach farm in 10 moves
				  # float so step's may include decimals

class Zombie(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self) # allows add sprite to a Sprite Group
		self.size = (50,50)
		self.surf = pygame.image.load('zombie.png').convert_alpha()
		self.surf = pygame.transform.scale(self.surf, self.size)

		# Other attributes
		self.MAX_HP = max_hp # used for health bars 
		self.health = max_hp # starting health
		self.step = None # This gets calculated when 'spawn' def is called
		self.location = [None, None]
		self.at_farm = False

	def take_damage(self, hit_damage):
		self.health -= hit_damage # zombie got hit, decrement HP

	def spawn(self, screen):
		# Choose a border/side: 1(top), 2(right), 3(bottom), 4(left)
		side = random.randint(1, 4)
		# Choose exact x or y coord (for top left of sq) based on which side
		rand_pos = random.randint(0, 750)
		if side == 1: # TOP
			spawn_loc = (rand_pos, 0)
		if side == 2: # RIGHT
			spawn_loc = (750, rand_pos)
		if side == 3: # BOTTOM
			spawn_loc = (rand_pos, 750)
		if side == 4: # LEFT
			spawn_loc = (0, rand_pos)

		# Calculate slope (direction) for this zombie and assign to step
		x_dist = (center[0] - spawn_loc[0]) # x dest - x spawn location
		y_dist = (center[1] - spawn_loc[1]) # y dest - y spawn location
		self.step = ((x_dist / speed), (y_dist / speed))

		self.super_Zombie() # random chance this zombie will be super 

		# Now the zombie in the randomly chosen border location
		screen.blit(self.surf, spawn_loc)

		# Now account the location of the zombie
		self.location[0] = spawn_loc[0]
		self.location[1] = spawn_loc[1]

	def super_Zombie(self):
		r = random.randint(1, 10)
		if r == 1: # 10% chance zombie will spawn into a super zombie
			self.size = super_zombie_size
			self.health = super_zombie_hp
			self.MAX_HP = super_zombie_hp
			self.surf = pygame.transform.scale(self.surf, self.size) # makes sure graphic is now large too

	# moves zombie one step closer to the farm
	def move(self, screen):
		self.location[0] += self.step[0]
		self.location[1] += self.step[1]

		screen.blit(self.surf, (self.location[0], self.location[1])) # draw zombie graphics on screen 
		self.health_bar(screen) # draw zombie's health bar (current instance zombie, so self)

	# draws zombie on the screen (does not move its location)
	def stay_put(self, screen):
		screen.blit(self.surf, (self.location[0], self.location[1]))
		self.health_bar(screen)

	# displays current HP of zombie in form of a health bar
	def health_bar(self, screen): # Rect Args: Surface to draw on, color, (xcoord,ycoord,width,height)
		# DRAW RED
		hb_loc = [self.location[0]+7, self.location[1]-7] # make hb a little above each zombie
		hb_dims = [zombie_size[0], 5] # width, height in pxs
		pygame.draw.rect(screen, red, (hb_loc, hb_dims)) # health lost is red

		# DRAW GREEN 
		# green rect ratio: green_width/red_width == current_hp/max_hp
		green_width = (self.health / self.MAX_HP) * zombie_size[0]
		pygame.draw.rect(screen, green, (hb_loc, [green_width, 5])) # health left is green

	# returns Rect object of zombie (xcoord, ycoord, width, height)
	def get_rectangle(self): 
		return pygame.Rect((self.location[0], self.location[1]), zombie_size)







