import pygame

red = (255, 0, 0)
green = (0, 255, 0)
barn_size = (100, 100) # width, height 
max_hp = 10
farm_center = (350, 350) # This is coord of farm's top left point (x, y)

class Farm(pygame.sprite.Sprite):
	def __init__(self):
		# object's surface is a farm png file 
		self.surf = pygame.image.load('barn.png').convert_alpha() # alpha for transparency 
		self.surf = pygame.transform.scale(self.surf, barn_size)

		# Other attributes
		self.health = max_hp # farm starts with 10 health

	def take_damage(self, hit_damage):
		self.health -= hit_damage # zombie reached the farm, reduces health by one

	def draw_graphics(self, screen):
		screen.blit(self.surf, farm_center) # draws farm onto screen 
		self.health_bar(screen) # draw farm's current hp

	# displays current HP of zombie in form of a health bar
	def health_bar(self, screen): # Rect Args: Surface to draw on, color, (xcoord,ycoord,width,height)
		# DRAW RED
		hb_loc = [farm_center[0], farm_center[1] + barn_size[1] + 5] # make hb a little above each zombie
		hb_dims = [barn_size[0], 5] # width, height in pxs
		pygame.draw.rect(screen, red, (hb_loc, hb_dims)) # health lost is red

		# DRAW GREEN 
		# green rect ratio: green_width/red_width == current_hp/max_hp
		green_width = (self.health / max_hp) * barn_size[0]
		pygame.draw.rect(screen, green, (hb_loc, [green_width, 5])) # health left is green

	# returns Rect object of zombie (xcoord, ycoord, width, height)
	def get_rectangle(self): 
		return pygame.Rect(farm_center, barn_size)








