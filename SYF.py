import pygame
from pygame.locals import * # add constants and functions of pygame
						    # into the global namespace of your file
import random 
import zombie as z 
import farm as f

# SAVE YOUR FARM!
# Save your family's beloved farm from spooky zombies by clicking
# them to death 
#
# Alexis Brown

# VARIABLES
display_width = 800 # pixels
display_height = 800
spawn_speed = 5000 # in milliseconds 
zombie_move_timer = 10 # zombies move every 100 milliseconds 
zombie_swing_timer = 1000 
zombies_at_farm = 0
player_hit_damage = 1
lower_spawn_timer_timer = 10000 # every 10 seconds, event happens to signal should lower spawn
								# timer, so longer player survives more zombies spawn

# COLORS
red = (255, 0, 0)
black = (0, 0, 0)
white = (255, 255, 255)
purple = (204, 153, 255)

pygame.init() # start up all pygame modules that you need
pygame.font.init() 

# DRAW THE BACKGROUND
game_display = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption("Save Your Farm!")

# Create grass image for background of the whole game 
grass = pygame.image.load('grass.png').convert()
grass = pygame.transform.scale(grass, (800,800))

# Create objects from personal imported class modules
farm = f.Farm()

# CUSTOM GAME EVENTS 
# pygame sees events as ints, the last event pygame saw is USEREVENT, so + 1 to make it unique!
ADDENEMY = pygame.USEREVENT + 1 # Set a timer for this event to happen (for the event queue to notice this)
pygame.time.set_timer(ADDENEMY, spawn_speed) # time in milliseconds 

MOVEZOMBIES = pygame.USEREVENT + 2 # signals to move zombies one more step toward the farm 
pygame.time.set_timer(MOVEZOMBIES, zombie_move_timer)

SWING = pygame.USEREVENT + 3 # how often all zombies will swing while at the farm
pygame.time.set_timer(SWING, zombie_swing_timer)

LOWERSPAWNTIMER = pygame.USEREVENT + 4 # increases rate zombies spawn 
pygame.time.set_timer(LOWERSPAWNTIMER, lower_spawn_timer_timer)

# TIME
clock = pygame.time.Clock() # Manages framerate for the game display 

# SPRITE GROUP
zombies = pygame.sprite.Group()

# graphics for the first couple seconds of the game (these get redrawn when zombies move)
game_display.blit(grass, (0,0)) 
farm.draw_graphics(game_display)

running = True
while running:
	# DRAW GRAPHICS
	farm.draw_graphics(game_display) # draws farm display in very center

	# Event handler, looks at every event in the queue
	for event in pygame.event.get():

		# 'q' or 'X' button to quit
		# User hit 'X' close button
		if event.type == QUIT:
			pygame.quit() # uninitializes all pygame modules
			quit() # exit the program

		# User hit a key
		if event.type == KEYDOWN:

			# User hit the Escape key, wants to exit
			if event.key == K_ESCAPE:
				pygame.quit() 
				quit() 

		# Did the User click (a left click so event button 1)
		if event.type == MOUSEBUTTONDOWN and event.button == 1:

			for zombie in zombies: # check every zombie 
				# User clicked, check if it was over a zombie
				# Get zombie's origin point's coords
				x = int(zombie.location[0]) # Convert to int
				y = int(zombie.location[1])
				# x, y, width, height --> same as a zombie
				area = pygame.Rect((x, y), zombie.size)

				# is event.pos (mouse click) in new rect area?
				if area.collidepoint(event.pos):
					zombie.take_damage(player_hit_damage)

					# did this hp dec kill this zombie?
					if zombie.health < 1:
						# before kill, check attributes, will this remove a zombie that reached the farm?
						if zombie.at_farm == True:
							zombies_at_farm -= 1
						zombie.kill() # remove zombie from the zombie sprite group 


		if event.type == ADDENEMY: # timer set off an add enemy event
			new_zombie = z.Zombie() # newly constructed Zombie object 
			zombies.add(new_zombie) # add new zombie to zombie object Group 
			new_zombie.spawn(game_display)

		if event.type == LOWERSPAWNTIMER:
			spawn_speed = int(spawn_speed / 2) # spawn speed gets halved, int conversion, set_timer cannot accept float value
			# need to re-assign the timer for the EVENT, otherwise will just keep original spawn_speed's value
			ADDENEMY = pygame.USEREVENT + 1 
			pygame.time.set_timer(ADDENEMY, spawn_speed)

		# event to move all the zombies one step closer to the farm
		if event.type == MOVEZOMBIES:
			# draw over old zombies' positions (so looks like theyre not there... when zombies actually
			game_display.blit(grass, (0,0))   # move, their location fields have their actual locations
			farm.draw_graphics(game_display)

			farm_rect = farm.get_rectangle() # Rect object representation of the farm, size and loc on the screen
			for zombie in zombies:
				zombie_rect = zombie.get_rectangle()
				# can check Sprite collision by using Rect collision method because farm and zombies are squares 
				collision = zombie_rect.colliderect(farm_rect)

				if collision: # zombie not at farm yet
					zombie.stay_put(game_display)

					if zombie.at_farm == False: # zombie just reached the farm, +1 to total zombies at the farm 
						zombies_at_farm += 1
						zombie.at_farm = True

				else: # no collision, zombie needs to move 
					zombie.move(game_display)

					# farm took damage, did it kill the farm? 
					if farm.health == 0: 
						running = False # exit main game loop

		# SWING EVENT. Zombies at the farm all take a swing 
		if event.type == SWING:
			farm.take_damage(zombies_at_farm)
			# Did this kill the farm?
			if farm.health < 1: # accounts for negative health
				running = False

	# game is over, running is false, should we keep it that way? 
	if running == False: 
		font = pygame.font.Font(None, 40)
		textsurface = font.render("QUIT [q] RESTART [r]", False, white) # RGB, color black 

		play_again = False
		while play_again == False:
			# GAME OVER SCREEN GRAPHICS
			game_display.fill(purple)
			gameover = pygame.image.load('gameover.png').convert_alpha()
			gameover = pygame.transform.scale(gameover, (400, 400))
			game_display.blit(gameover, (400, 400)) # show graphics middle of screen
			game_display.blit(textsurface, (460,700))

			# wait for events again, user response to quitting or restarting the game 
			for event in pygame.event.get():

				# 'q' or 'X' button to quit
				# User hit 'X' close button
				if event.type == QUIT:
					pygame.quit() # uninitializes all pygame modules
					quit() # exit the program

				# User hit a key
				if event.type == KEYDOWN:

					# User hit the Escape key, wants to exit
					if event.key == K_ESCAPE:
						pygame.quit() 
						quit() 

					# user wants to quit 
					if event.key == K_q:
						pygame.quit() 
						quit() 

					# user hit the 'r' key, they want to restart the game 
					if event.key == K_r:
						# reset variables to have a fresh game
						running = True
						# re-construct objects for new attributes 
						farm = f.Farm()
						zombies = pygame.sprite.Group() 
						zombies_at_farm = 0
						# Reset spawn scale so doesnt begin with same spawn rate as the game exited with 
						spawn_speed = 5000
						ADDENEMY = pygame.USEREVENT + 1 # Set a timer for this event to happen (for the event queue to notice this)
						pygame.time.set_timer(ADDENEMY, spawn_speed) # time in milliseconds 

						# display fresh game again, before the 5s before first zombie spawns
						game_display.blit(grass, (0,0)) 
						farm.draw_graphics(game_display)
						play_again = True # exit this small while loop to go back to major while loop 

			# display graphics for the game over screen's while loop 
			pygame.display.flip() 

	# Update the display
	pygame.display.flip()

	clock.tick(30) # 30 FPS




